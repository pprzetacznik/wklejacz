package com.wklejacz;

import java.io.IOException;

import us.monoid.web.JSONResource;
import us.monoid.web.Resty;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.wklejacz.R.string;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class LoginActivity extends Activity {
	public static final String PREFS_NAME = "LoginPrefs";

	private UserLoginTask mAuthTask = null;

	private String mLogin;
	private String mPassword;

	// UI references
	private EditText mLoginView;
	private EditText mPasswordView;
	private static String baseCredentials;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);

		// Set up the login form.
		mLoginView = (EditText) findViewById(R.id.email);

		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							attemptLogin();
							return true;
						}
						return false;
					}
				});

		findViewById(R.id.login_form);
		findViewById(R.id.login_status);

		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptLogin();
					}
				});
	}

	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors
		mLoginView.setError(null);
		mPasswordView.setError(null);

		// Store values
		mLogin = mLoginView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Checks if fields not empty
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		}

		if (TextUtils.isEmpty(mLogin)) {
			mLoginView.setError(getString(R.string.error_field_required));
			focusView = mLoginView;
			cancel = true;
		}

		if (cancel) {

			focusView.requestFocus();
		} else {
			showProgress();
			mAuthTask = new UserLoginTask();
			mAuthTask.execute((Void) null);
		}
	}

	@SuppressWarnings("deprecation")
	protected void hideProgress() {
		dismissDialog(0);
	}

	@SuppressWarnings("deprecation")
	protected void showProgress() {
		showDialog(0);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		final ProgressDialog dialog = new ProgressDialog(this);
		dialog.setMessage(getText(string.login_progress_signing_in));
		dialog.setIndeterminate(true);
		dialog.setCancelable(true);
		dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			public void onCancel(DialogInterface dialog) {
				if (mAuthTask != null)
					mAuthTask.cancel(true);
			}
		});
		return dialog;
	}

	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {
			String credentials = mLogin + ":" + mPassword;
			baseCredentials = Base64.encodeToString(credentials.getBytes(),
					Base64.DEFAULT);
			//User.setBaseCredentials(baseCredentials);
			//User.setLogin(mLogin);
			Resty r = new Resty();
			r.withHeader("Authorization", "Basic " + baseCredentials);

			JSONResource son;
			try {
				son = r.json(MainActivity.getServerAddress() + "?format=json");
			} catch (IOException e) {
				son = null;
				e.printStackTrace();
			}

			return (son != null);
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress();

			if (success) {

				// put encoded login:password into SharedPreferences
				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putString("logged", baseCredentials);
				editor.commit();

				Intent intent = new Intent(LoginActivity.this,
						MainActivity.class);
				startActivity(intent);
				finish();
			} else {
				mPasswordView
						.setError(getString(R.string.error_incorrect_password));
				mPasswordView.requestFocus();
				hideProgress();
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			hideProgress();
		}
	}

}
