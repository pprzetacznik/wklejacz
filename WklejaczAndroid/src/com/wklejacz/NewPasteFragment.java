package com.wklejacz;

import java.util.concurrent.ExecutionException;

import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;
import android.content.ClipboardManager;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.wklejacz.tasks.NewPasteRestTask;

public class NewPasteFragment extends Fragment {

	private JSONObject json = new JSONObject();
	private String description;
	private String content;
	private String base;
	ClipboardManager clipboard;
	EditText cont;
	EditText desc;
	Spinner typeSpinner;

	public NewPasteFragment() {
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.new_paste, container, false);
		Button button = (Button) rootView.findViewById(R.id.addButton);
		this.setHasOptionsMenu(true);

		// paste type spinner
		typeSpinner = (Spinner) rootView.findViewById(R.id.type_spinner);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				getActivity(), R.array.type_array,
				android.R.layout.simple_spinner_item);

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		typeSpinner.setAdapter(adapter);

		// EditTexts references
		cont = (EditText) rootView.findViewById(R.id.paste);
		desc = (EditText) rootView.findViewById(R.id.descriptionNew);

		// The Add button listener
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				cont.setError(null);
				desc.setError(null);
				boolean cancel = false;
				View focusView = null;
				content = cont.getText().toString();
				description = desc.getText().toString();
				if (TextUtils.isEmpty(content)) {
					cont.setError(getString(R.string.error_field_required));
					focusView = cont;
					cancel = true;
				}

				if (TextUtils.isEmpty(description)) {
					desc.setError(getString(R.string.error_field_required));
					focusView = desc;
					cancel = true;
				}

				if (cancel) {

					focusView.requestFocus();
				} else {

					try {
						json.put("description", description);
						json.put("content", content);
						json.put("type_id", typeSpinner.getSelectedItem()
								.toString());
					} catch (JSONException e) {

						e.printStackTrace();
					}
					SharedPreferences settings = getActivity()
							.getSharedPreferences(LoginActivity.PREFS_NAME, 0);
					base = settings.getString("logged", "NOTLOGGED").toString();

					AsyncTask<Object, String, Boolean> task = new NewPasteRestTask()
							.execute(json, base);
					Boolean result = true;
					try {
						result = task.get();
					} catch (InterruptedException e) {

						e.printStackTrace();
					} catch (ExecutionException e) {

						e.printStackTrace();
					}
					CharSequence text;
					if (result) {
						text = "Wlejka została dodana";
					} else
						text = "Bład przy dodawaniu wklejki";
					int duration = Toast.LENGTH_SHORT;

					Toast toast = Toast.makeText(getActivity(), text, duration);
					toast.show();

				}
			}
		});

		return rootView;
	};

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Add your menu entries here
		super.onCreateOptionsMenu(menu, inflater);
		MenuItem clean = menu.add("Wyczyść");

		clean.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

			@Override
			public boolean onMenuItemClick(MenuItem item) {
				cont.setText("");
				desc.setText("");
				return true;
			}
		});

	}

}
