package com.wklejacz;

import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.wklejacz.tasks.PasteDetailRestTask;

public class PasteActivity extends Activity {

	private Paste paste;
	private String content = null;
	private String description;
	private String base;
	
	private Float textSize;
	
	private static final float INITIAL_TEXT_SIZE = 14;

	TextView contentView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_paste);

		Intent intent = getIntent();
		Button button = (Button) findViewById(R.id.copyButton);

		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				copyContent();

			}
		});
		textSize = INITIAL_TEXT_SIZE;
		paste = (Paste) intent.getSerializableExtra("paste");
		content = paste.getContent();
		description = paste.getDescription();
		this.setTitle(description);
		contentView = (TextView) findViewById(R.id.content);
		contentView.setTextSize(textSize);
		// /////check if content has been downloaded already
		if (content == null) {
			// get authorization,
			SharedPreferences settings = getSharedPreferences(
					LoginActivity.PREFS_NAME, 0);
			base = settings.getString("logged", "NOTLOGGED").toString();
			// get content
			AsyncTask<String, Void, String> task = new PasteDetailRestTask()
					.execute(paste.getLink(), base);

			try {
				content = task.get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (content != null) {
			// EditText editText = (EditText) findViewById(R.id.pasteContent);
			
			TextView descView = (TextView) findViewById(R.id.description);
			descView.setText(description);
			contentView.setText(content);

			paste.setContent(content);

		} else {

			CharSequence text = "Błąd pobierania treści wklejki!";
			int duration = Toast.LENGTH_SHORT;

			Toast toast = Toast.makeText(getApplicationContext(), text,
					duration);
			toast.show();
			this.finish();
		}
		setupActionBar();
	}

	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.paste, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.itemCopy:
			copyContent();
			return true;
		case R.id.zoomIn:
			contentView.setTextSize(++textSize);
			return true;
		case R.id.zoomOut:
			contentView.setTextSize(--textSize);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void copyContent() {
		ClipboardManager clipboard = (ClipboardManager) getSystemService(Service.CLIPBOARD_SERVICE);
		ClipData clip = ClipData.newPlainText("paste", content);
		clipboard.setPrimaryClip(clip);
	}

}
