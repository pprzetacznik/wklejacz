package com.wklejacz;

import java.io.Serializable;
import java.util.Date;

public class Paste implements Serializable, Comparable<Paste>{

	private String description;
	private String link;
	private String content;
	private Date date;
	private String owner;
	private String type;
	
	public Paste(String des, String link)
	{
		this.description= des;
		this.link = link;
		this.content = null;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String toString() {
		return getDescription();
	}

	@Override
	public int compareTo(Paste another) {

		return description.compareTo(another.getDescription());
	}
	

}
