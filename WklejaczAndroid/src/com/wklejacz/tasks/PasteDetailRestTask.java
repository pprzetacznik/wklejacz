package com.wklejacz.tasks;

import com.wklejacz.MainActivity;

import us.monoid.json.JSONObject;
import us.monoid.web.JSONResource;
import us.monoid.web.Resty;
import android.os.AsyncTask;

public class PasteDetailRestTask extends AsyncTask<String, Void, String> {

	@Override
	protected String doInBackground(String... arg0) {
		Resty r = new Resty();
		r.withHeader("Authorization", "Basic "+arg0[1]);

		JSONResource son;
		JSONObject obj;

		try {
			son = r.json(MainActivity.getServerAddress()
					+ arg0[0] + "?format=json");
			obj = son.object();

			return obj.getString("content");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

}
