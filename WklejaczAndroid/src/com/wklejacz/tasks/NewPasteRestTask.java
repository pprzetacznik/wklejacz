package com.wklejacz.tasks;

import java.io.IOException;

import com.wklejacz.LoginActivity;
import com.wklejacz.MainActivity;


import us.monoid.json.JSONObject;
import us.monoid.web.Resty;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Base64;

public class NewPasteRestTask extends AsyncTask<Object, String, Boolean> {
	private boolean result;
	@Override
	protected Boolean doInBackground(Object... params) {
		Resty r = new Resty();

		String base = (String) params[1];

		r.withHeader("Authorization", "Basic "+base);

		try {
			r.json(MainActivity.getServerAddress(),
					Resty.content((JSONObject)params[0]));
			result=true;
		} catch (IOException e) {
			result=false;
			e.printStackTrace();
		}
		return result;
	}

}
