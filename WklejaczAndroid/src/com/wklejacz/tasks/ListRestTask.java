package com.wklejacz.tasks;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.wklejacz.LoginActivity;
import com.wklejacz.MainActivity;
import com.wklejacz.Paste;
import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;
import us.monoid.web.JSONResource;
import us.monoid.web.Resty;
import android.content.SharedPreferences;
import android.os.AsyncTask;

public class ListRestTask extends AsyncTask<String, Void, List<Paste>> {

	@Override
	protected List<Paste> doInBackground(String... params) {
				
		Resty r = new Resty();
		r.withHeader("Authorization", "Basic "+params[0]);
		JSONResource son;
		JSONArray array;

		Map<String, String> pastes = new HashMap<String, String>();
		List<Paste> pasteList = new LinkedList<Paste>();
		try {
			son = r.json(MainActivity.getServerAddress()+"?format=json");
			array = son.array();
			for (int i = 0; i < array.length(); i++) {
				JSONObject obj = array.getJSONObject(i);
				pastes.put(obj.getString("description"), obj.getString("link"));
				pasteList.add(new Paste(obj.getString("description"), obj.getString("link")));
			}
			return pasteList;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

}
