package com.wklejacz;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.wklejacz.tasks.ListRestTask;

public class PasteListFragment extends ListFragment {

	// Map<String, String> pastes;
	List<Paste> pastes;
	private String base;

	public PasteListFragment() {
	};

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// pastes = new TreeMap<String, String>();
		pastes = new LinkedList<Paste>();
		SharedPreferences settings = getActivity().getSharedPreferences(
				LoginActivity.PREFS_NAME, 0);
		base = settings.getString("logged", "NOTLOGGED").toString();

		if (this.refresh() == false) {
			CharSequence text = "Błąd pobierania listy! Spróbuj odświeżyć";
			int duration = Toast.LENGTH_SHORT;

			Toast toast = Toast.makeText(getActivity(), text, duration);
			toast.show();
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Intent myIntent = new Intent(getActivity(), PasteActivity.class);

		Paste paste = (Paste) getListView().getItemAtPosition(position);

		myIntent.putExtra("paste", paste);
		getActivity().startActivity(myIntent);

	}

	public boolean refresh() {
		AsyncTask<String, Void, List<Paste>> task = new ListRestTask()
				.execute(base);
		try {
			pastes = task.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (pastes != null) {
			// List<String> pastesDescriptions = new
			// ArrayList<String>(pastes.keySet());

			Collections.sort(pastes);
			ArrayAdapter<Paste> adapter = new ArrayAdapter<Paste>(
					getActivity(), android.R.layout.simple_list_item_1, pastes);
			setListAdapter(adapter);
			return true;
		} else
			return false;

	}

}
