from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns

from raw_entries import views

urlpatterns = patterns('',
    url(r'^$', views.NotesList.as_view(), name='index'),
    url(r'^(?P<entry_link>[a-z0-9]{32})/$', views.NotesDetails.as_view(), name='detail'),
    url(r'^api-token-auth/', 'rest_framework.authtoken.views.obtain_auth_token'),
)

urlpatterns = format_suffix_patterns(urlpatterns)