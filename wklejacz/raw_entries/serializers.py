from django.core import serializers
from django.forms import widgets
from rest_framework import serializers
from rest_framework.relations import RelatedField, SlugRelatedField
from entries.models import Notes, NoteTypes
from django.contrib.auth.models import User
from datetime import datetime
from hashlib import md5


# class DateTimeField(serializers.SlugRelatedField):
#     def to_native(self, value):
#         duration = datetime.strftime('%M:%S', time.gmtime(value.duration))
#         return 'Track %d: %s (%s)' % (value.order, value.name, duration)

class NoteTypesSerializer(serializers.ModelSerializer):
    class Meta:
        model = NoteTypes
        fields = ('name',)

class NotesSerializer(serializers.ModelSerializer):
    owner_id = serializers.SlugRelatedField(slug_field='username')
    type_id = serializers.SlugRelatedField(slug_field='name')

    def restore_object(self, attrs, instance=None):
        m = md5()
        date = datetime.now()
        m.update(date.isoformat())
        note = Notes(description=attrs.get('description', ''), content=attrs.get('content', ''), create_date=date, link=m.hexdigest(), owner_id=attrs.get('owner_id'), type_id=attrs.get('type_id'))
        return note

    class Meta:
        model = Notes
        fields = ('description', 'content', 'create_date', 'link', 'owner_id', 'type_id')
        read_only_fields = ('owner_id', 'link', 'create_date')