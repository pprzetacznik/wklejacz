from rest_framework import status
from rest_framework.response import Response
from entries.models import Notes
from raw_entries.serializers import NotesSerializer
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.authentication import SessionAuthentication, BasicAuthentication

class NotesList(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        notes = Notes.objects.filter(owner_id=request.user)
        serializer = NotesSerializer(notes, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        attrs = request.DATA
        attrs[u'owner_id'] = request.user.username
        serializer = NotesSerializer(data = attrs)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class NotesDetails(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, entry_link, format=None):
        try:
            note = Notes.objects.get(link=entry_link)
            if ( not (note.type_id.name == 'Publiczny') and not (note.type_id.name == 'Ukryty') and not ( (note.type_id.name == 'Prywatny') and (note.owner_id==request.user) ) ):
                return Response(status=status.HTTP_404_NOT_FOUND)
        except Notes.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = NotesSerializer(note)
        return Response(serializer.data)
