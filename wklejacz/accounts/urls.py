from django.conf.urls import patterns, url
from accounts import views

urlpatterns = patterns('',
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^$', 'django.contrib.auth.views.login', {'template_name': 'accounts/login.html'}),
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'accounts/login.html'}, name='login'),
    url(r'^register$', views.register, name='register')
)