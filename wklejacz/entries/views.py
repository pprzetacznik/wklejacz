from django.shortcuts import render, render_to_response, get_object_or_404, HttpResponseRedirect
from django.core.urlresolvers import reverse
from entries.models import Notes, NoteTypes
from django.core.context_processors import csrf
from datetime import datetime
from hashlib import md5
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.http import Http404

def index(request):
    if request.user.is_authenticated():
        notes_list = Notes.objects.filter(owner_id=request.user).order_by('-create_date')
        note_types = NoteTypes.objects.all()

        c = RequestContext(request, {'notes_list': notes_list, 'note_types': note_types})
        c.update(csrf(request))
        return render_to_response('entries/index.html', c)
    else:
        noteType = NoteTypes.objects.get(name='Publiczny')
        notes_list = Notes.objects.select_related().filter(type_id = noteType).order_by('-create_date')
        entry = notes_list[0]

        c = RequestContext(request, {'notes_list': notes_list, 'entry': entry})
        c.update(csrf(request))

        return render_to_response('entries/detail.html', c)

def detail(request, entry_link):
    if ( request.user.is_authenticated() ):
        notes_list = Notes.objects.filter(owner_id=request.user).order_by('-create_date')
    else:
        noteType = NoteTypes.objects.get(name='Publiczny')
        notes_list = Notes.objects.select_related().filter(type_id = noteType).order_by('-create_date')
    entry = get_object_or_404(Notes, link=entry_link)

    if ( (entry.type_id.name == 'Publiczny') or (entry.type_id.name == 'Ukryty') ):
        return render(request, 'entries/detail.html', {'entry': entry, 'notes_list': notes_list})
    else:
        if ( request.user.is_authenticated() and ( (entry.type_id.name == 'Prywatny') and (entry.owner_id==request.user) ) ):
            return render(request, 'entries/detail.html', {'entry': entry, 'notes_list': notes_list})
    raise Http404

@login_required
def add(request):
    if not request.POST['description'] or not request.POST['content'] or not request.POST['note_type']:
        notes_list = Notes.objects.filter(owner_id=request.user)
        note_types = NoteTypes.objects.all()
        c = {'notes_list': notes_list, 'note_types': note_types, 'error_message': "Nie podales opisu lub zawartosci wpisu"}
        c.update(csrf(request))

        return render(request, 'entries/index.html', c)

    m = md5()
    date = datetime.now()
    m.update(date.isoformat())

    user = request.user
    noteType = NoteTypes(id=request.POST['note_type'])
    note = Notes(description=request.POST['description'], content=request.POST['content'], create_date=date, link=m.hexdigest(), owner_id=user, type_id=noteType)
    note.save()

    return HttpResponseRedirect(reverse('entries:index'))