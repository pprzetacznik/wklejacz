from django.db import models
from django.contrib.auth.models import User

class NoteTypes(models.Model):
    name = models.CharField( max_length=30, unique=True )
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name = 'Note Type'
        verbose_name_plural = 'Note Types'

class Notes(models.Model):
    description = models.CharField( max_length=255 )
    content = models.TextField()
    create_date = models.DateTimeField()
    link = models.CharField( max_length=32, unique=True )
    owner_id = models.ForeignKey( User, related_name='notes' )
    type_id = models.ForeignKey( NoteTypes, related_name='notes' )
    class Meta:
        verbose_name = 'Note'
        verbose_name_plural = 'Notes'