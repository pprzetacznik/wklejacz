from django.conf.urls import patterns, url

from entries import views

from django.views.generic import TemplateView

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^(?P<entry_link>[a-z0-9]{32})/$', views.detail, name='detail'),
    url(r'^add/$', views.add, name='add'),
    url(r'^about/$', TemplateView.as_view(template_name="entries/about.html"), name='about'),
    url(r'^contact/$', TemplateView.as_view(template_name="entries/contact.html"), name='contact'),
)