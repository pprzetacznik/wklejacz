from django.conf.urls import patterns, include, url
from django.views.generic import DetailView, ListView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from entries.models import Notes, NoteTypes

admin.autodiscover()

class NoteTypesAdmin( admin.ModelAdmin ):
    fields = ['name']
    # list_display = ('name')

class NotesAdmin( admin.ModelAdmin ):
    fields = ['description', 'content', 'create_date', 'link', 'owner_id', 'type_id']
    list_display = ('description', 'content', 'create_date', 'link', 'owner_id', 'type_id')

admin.site.register( NoteTypes, NoteTypesAdmin )
admin.site.register( Notes, NotesAdmin )

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'wklejacz.views.home', name='home'),
    # url(r'^wklejacz/', include('wklejacz.foo.urls')),
    url(r'^', include('entries.urls')),
    url(r'^entries/', include('entries.urls', namespace='entries')),
    url(r'^raw_entries/', include('raw_entries.urls', namespace='raw_entries')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^accounts/', include('accounts.urls', namespace='accounts')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
