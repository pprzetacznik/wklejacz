import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DetailPage {
    private final WebDriver driver;
    
    @FindBy(xpath="//div[@class='hero-unit']/h2")
    WebElement description;
    
    @FindBy(xpath="//div[@class='hero-unit']/p")
    WebElement content;
    
    public DetailPage(WebDriver driver) {
        this.driver = driver;
        
        PageFactory.initElements(driver, this);
    }
}