import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestScenario {

	WebDriver driver = new FirefoxDriver();
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		driver.manage().window().maximize();
        driver.get("http://users.dsnet.agh.edu.pl:8759/entries/");
        
        MainPage mainPage = new MainPage(driver);
        mainPage.login.click();
        
        LoginPage loginPage = new LoginPage.Builder()
        	.withUsername("piotrek")
        	.withPassword("b")
        	.build(driver);
        
        loginPage.button.click();
        
        String content = "Nowy test" + getTimestamp();
        String description = "New description" + getTimestamp();
        
        mainPage = new MainPage.Builder()
        	.withDescription( description )
        	.withContent( content )
        	.withType( "2" )
        	.build(driver);
        
        mainPage.button.click();
        
        WebElement entry = null;
        boolean exists = false;
        for( WebElement e : mainPage.entriesDescriptions ) {
        	if ( e.getText().contains( description ) ) {
        		entry = e;
        		exists = true;
        		System.out.println( entry.getText() );
        	}
        }
        assertTrue( exists );
        
        entry.click();
        
        DetailPage detailPage = new DetailPage(driver);
        assertTrue( detailPage.content.getText().contains(content) );
        assertTrue( detailPage.description.getText().contains(description) );
        
        System.out.println("Page title is: " + driver.getTitle());
	}
	
	@After
	public void turnDown() {
		driver.quit();
	}
	
	public String getTimestamp() {
		return new Timestamp(System.currentTimeMillis()).toString();
	}
}
