import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage {
    private final WebDriver driver;
    
    @FindBy(xpath="//form//input[@name='description']")
    WebElement description;
    
    @FindBy(xpath="//form/textarea[@name='content']")
    WebElement content;
    
    @FindBy(xpath="//form/select[@name='note_type']")
    WebElement type;
    
    @FindBy(xpath="//form/input[@type='submit']")
    WebElement button;
    
    @FindBy(xpath="//ul/li/a")
    List<WebElement> entriesDescriptions;
    
    @FindBy(xpath="//a[contains(text(), 'Zaloguj')]")
    WebElement login;

    public MainPage(WebDriver driver) {
        this.driver = driver;
        
        PageFactory.initElements(driver, this);
    }
    
    static class Builder {
    	String description = "default";
    	String content = "default";
    	String type = "1";
    	
    	public MainPage build(WebDriver driver) {
    		MainPage mainPage = new MainPage(driver);
    		mainPage.description.sendKeys( description );
    		mainPage.content.sendKeys( content );
    		mainPage.type.sendKeys( type );
    		
    		return mainPage;
    	}
    	
    	public Builder withDescription( String description ) {
    		this.description = description;
    		return this;
    	}
    	
    	public Builder withContent( String content ) {
    		this.content = content;
    		return this;
    	}
    	
    	public Builder withType( String type ) {
    		this.type = type;
    		return this;
    	}
    }
}