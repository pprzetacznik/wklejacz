import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    private final WebDriver driver;
    
    @FindBy(xpath="//form//input[@name='username']")
    WebElement user;
    
    @FindBy(xpath="//form//input[@name='password']")
    WebElement password;
    
    @FindBy(xpath="//form//input[@type='submit']")
    WebElement button;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        
        PageFactory.initElements(driver, this);
    }
    
    static class Builder {
    	String user = "admin";
    	String password = "b";
    	String type = "1";
    	
    	public LoginPage build(WebDriver driver) {
    		LoginPage loginPage = new LoginPage(driver);
    		loginPage.user.sendKeys( user );
    		loginPage.password.sendKeys( password );
    		
    		return loginPage;
    	}
    	
    	public Builder withUsername( String value ) {
    		this.user = value;
    		return this;
    	}
    	
    	public Builder withPassword( String value ) {
    		this.password = value;
    		return this;
    	}
    }
}